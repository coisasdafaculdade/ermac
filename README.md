#Acesso ao servidor
Para acessar o servidor use a URL ``` localhost/ermac/server ```
As requisições ao servidor, deverão ser feitas para a seguinte URL ``` localhost/ermac/server/index.php ```

#Especificação do JSON

Objeto ERMAC
------------
```
{
  x : 123,
  y : 123,
  width : 30,
  height: 40,
  type : "entity"
  text : "Coisa",
  line : [],
  constraint : [],
  datatype   : "VARCHAR",
  relatedTo  : null,
  attributes : [],
  relatedToObject : null,
  mask : null
}
```


**Atributo**

``` x, y ```

**Descrição**

Definem as coordenadas do objeto no canvas

**Possiveis valores**

* Menor valor possivel é 0 + obj.width
* Maior valor possivel é canvas.width - obj.width

**Questões em aberto:**

Os possiveis valores serão os especificados?

--------------------------------------------------
**Atributo**

``` relatedTo ```

**Descrição**

Define o nome da entidade o objeto esta relacionado. Assume ``` null ``` se nao houver nenhum relacionamento.
Se a entidade pai for renomeada, simplesmente deve-se percorrer o vetor de ``` attributes ``` e atualizar o campo ``` relatedTo ``` de seus filhos.

**Possiveis valores**

* Mantem o ``` text ``` do entidade pai relacionada.

**Questões em aberto:**

--------------------------------------------------
**Atributo**

``` relatedToObject ```

**Descrição**

Define o objeto da entidade o objeto esta relacionado. Assume ``` {} ``` se nao houver nenhum relacionamento.

**Possiveis valores**

* {} se não tem ninguém relacionado
*[Object objeto] se tiver o objeto relacionado

--------------------------------------------------
**Atributo**

``` mask ```

**Descrição**

Define o o objeto da máscara onde o elemento está contido.

--------------------------------------------------
**Atributo**

``` width, height ```

**Descrição**

Definem as dimensoes do objeto no canvas

**Possiveis valores**

* Menor valor possivel é 10
* Maior valor possível é 150

**Questões em aberto**

Os possiveis valores serão os especificados?

--------------------------------------------------
**Atributo**

``` type ```

**Descrição**

Define o tipo do objeto criado no canvas.

**Possiveis valores**

* ```"entity"```        - Descreve uma entidade
* ```"attribute"```     - Descreve um atributo (de uma entidade)
* ```"relationship"```  - Descreve um relacionamento sem entidades relacionadas
* ```"many-to-many"```  - Descreve um relacionamento muitos pra muitos
* ```"one-to-many"```  - Descreve um relacionamento um pra muitos
* ```"one-to-one"```  - Descreve um relacionamento um pra um

**Questões em aberto:**

A quantidade de possiveis valores especificadas é suficiente?

--------------------------------------------------
**Atributo**

``` text ```

**Descrição**

Texto escrito no objeto

**Possiveis valores**

* Conter as mesmas restrições de nomes do SQL

**Questões em aberto:**

Limitar caracteres ?

--------------------------------------------------
**Atributo**

``` line ```

**Descrição**

Vetor de linhas que sai do objeto.

**Possiveis valores**
    
  Para attributes, a linha sairá do attribute e se ligará a entidade/relacionamento especificado
  Para entity, a linha sairá da entity e se ligará ao relacionamento especificado
  Uma linha nunca sairá de um relationship

**Questões em aberto:**

Cada linha também é um objeto Ermac?

Este atributo é necessário?

--------------------------------------------------
**Atributo**

``` constraint ```

**Descrição**

Determina quais os tipos de restrições se aplica ao objeto.
Um atributo pode conter dois tipos de ``` constraint ``` simultaneamente ``` "primary-key", "foreign-key" ````, mas se definido como ``` "derived" ``` deve ser atributo unico.

**Possiveis valores**

* ``` [] ```       - Descreve um objeto o qual não sofre de nenhuma restrição
* ``` "primary-key" ```   - Descreve um objeto que é chave primaria de uma relação
* ``` "multivalued" ```   - Descreve um objeto multivalorado
* ``` "derived" ```       - Descreve um objeto derivado

**Questões em aberto:**

Objetos mutivalorados requerem uma tabela separada e uma chave estrageira na
relação atual. Essas tabelas serão geradas automaticamente pelo JSONBuilder ou
ja deverão obrigatoriamente ja existirem no canvas?

--------------------------------------------------
**Atributo**

``` datatype ```

**Descrição**

Determina qual o tipo de dado o determinado objeto se refere.
Atributo foi criado para facilitar o mapeamento para SQL.
Atrubuto de uso exclusivo somente para objetos do tipo "attribute"

**Possiveis valores**

* ``` "VARCHAR" ```
* ``` "INT" ```
* ``` "DECIMAL" ```
* ``` "BOOLEAN" ```

**Questões em aberto:**

Todos os possiveis valores são essenciais?

--------------------------------------------------
**Atributo**

``` attributes ```

**Descrição**

Buscas armazenar todos os outros objetos relacionados ao objeto em questão

**Possiveis valores**

* Para o tipo ``` "entity" ```        - Armazena os atributos da entidade
* Para o tipo ``` "attribute" ```     - Armazena a tabela relacionada à chave estrangeira na primeira posição, e a coluna associada na segunda posição (Convensão)
* Para o tipo ``` "relationship" ```  - Vazio
* Para o tipo ``` "one-to-one" ```  - Na posição zero armazena entidade one que terá a chave estrangeira da entidade na posiçao 1.
* Para o tipo ``` "one-to-many" ```  - Na posição zero armazena entidade one.Na posição um armazena a entidade many.
* Para o tipo ``` "many-to-many" ```  - Na posição zero armazena entidade da esquerda.Na posição um armazena a entidade da direita (Convensão)

**Questões em aberto:**
    -

--------------------------------------------------
**Observações**

 -- nenhuma --