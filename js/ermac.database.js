function saveDiagram() {
  	$.post('http://localhost/ermac/server/request.php?CREATE',
    {'json':jsonBuilder(canvas.children)},
    function(data, status) {
		// console.log(data);
    });
}

function selectDiagram() {
	$.post('http://localhost/ermac/server/request.php?SELECT',
    function(data,  status) {
        var json = [];
        var diagramas = data.split("\n");
        
        for(var i = 2; i < diagramas.length-1; i++){
            var cols = diagramas[i].split(";");
            json.push(JSON.parse(cols[2].replace(/'/g, "")));
        }

        // desenha no canvas
        var i = 4;
        // for(var i = 0; i < json.length; i++) {
            canvas.reset();
            for (var j = 0; j < json[i].length; j++) {
                if (json[i][j].type === "attribute") {
                    var obj = componentFactory.recreateAttribute(json[i][j]);
                    canvas.addChild(obj);
                }
                else if (json[i][j].type === "entity") {
                    var obj = componentFactory.recreateEntity(json[i][j]);
                    canvas.addChild(obj);
                }
                else {
                    var obj = componentFactory.recreateRelationship(json[i][j]);
                    canvas.addChild(obj);
                }
            }
            canvas.redraw();
        // }

    }, "text");
}

function deleteDiagram(id) {
	$.post('http://localhost/ermac/server/request.php?DELETE=' + id,
    function(data,  status) {
        // console.log(data);
    }, "text");
}

$(document).ready(function(){
	// var result = deleteDiagram(1);
});