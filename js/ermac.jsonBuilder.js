
function jsonBuilder(components) {
  var json = [];

  for(var i = 0; i < components.length; i++) {
    var object = components[i].__proto__.children[0];

    if (components[i].type !== "line") {

      json.push({
        x : components[i].x,
        y : components[i].y,
        width : components[i].width,
        height: components[i].height,
        type : object.ermac.type,
        text : object.ermac.text,
        constraint : object.ermac.constraint,
        datatype : object.ermac.datatype,
        relatedTo : object.ermac.relatedTo
      });
    }

  }

  return json;
}