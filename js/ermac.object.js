function Ermac(text, type, mask) {
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0
    /**
     * Define o tipo do objeto criado no canvas.
     * Possiveis valores: "entity", "attribute", "relationship", "many-to-many",
     * "one-to-many", "one-to-one"
     * 
     * @type String
     */
    this.type = type;
    /**
     * O título do elemento no canvas.
     * @type String
     */
    this.text = text;
    this.line = [];
    /**
     * Determina quais os tipos de restrições se aplica ao objeto.
     * Um atributo pode conter dois tipos de constraint simultaneamente 
     * "primary-key", "foreign-key" , mas se definido como "derived" deve ser 
     * atributo único.
     * 
     * Possiveis valores: [],"primary-key","multivalued","derived"
     * 
     * @type String
     */
    this.constraint = "default";
    /**
     * Determina qual o tipo de dado o determinado objeto se refere.
     * Atributo foi criado para facilitar o mapeamento para SQL. 
     * Atrubuto de uso exclusivo somente para objetos do tipo "attribute".
     * Possiveis valores: "VARCHAR","INT","DECIMAL","BOOLEAN".
     * @type String
     */
    this.datatype = "VARCHAR";
    /**
     * Procura armazenar todos os outros objetos relacionados ao objeto em questão
     * 
     * Para o tipo "entity" - Armazena os atributos da entidade
     * Para o tipo "attribute" - Armazena a tabela relacionada à chave estrangeira na primeira posição, e a coluna associada na segunda posição (Convensão)
     * Para o tipo "relationship" - Vazio
     * Para o tipo "one-to-one" - Na posição zero armazena entidade one que terá a chave estrangeira da entidade na posiçao 1.
     * Para o tipo "one-to-many" - Na posição zero armazena entidade one.Na posição um armazena a entidade many.
     * Para o tipo "many-to-many" - Na posição zero armazena entidade da esquerda.Na posição um armazena a entidade da direita (Convensão)
     * @param vector[] Uma string com um dos possíveis valores
     */
    this.attributes = []; //Buscas armazenar todos os outros objetos relacionados ao objeto em questão
    this.relatedTo = "none";
    this.relatedToObject = null;
    this.mask = mask;
}