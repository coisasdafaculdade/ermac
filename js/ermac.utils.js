
function canvasToImage() {
	var canvas = document.getElementById("canvas");
	window.open(canvas.toDataURL("image/png"));
}

function resetCanvas() {
    canvas.reset()
    selectorArea = null;
    selectorEnabled = false;
    selectorDragging = false;
    selection = null;
    selectionEnabled = false;
    selectionDragging = false;
    selectedObjects = [];
}

$(function() {
  $canvas = $("#canvas");
  $janela = $("#canvasHolder");

  var redimensionaCanvas = function () {
    console.log($janela.width());
    console.log($janela.height());

    $canvas.attr("width", ($(window).width() * 0.79) + "px");
    $canvas.attr("height", $janela.height() + "px");
  }

  $(window).resize(redimensionaCanvas);

  redimensionaCanvas();
});



$(function() {

    $('#outer_container').PieMenu({
        'starting_angel': 0,
        'angel_difference' : 240,
        'radius': 60,
    });
    $("#outer_container").draggable();
    $("#outer_container").css("left" , $("#canvas").width() - 100);
    $("#outer_container").css("top"  , $("#canvas").height());
});