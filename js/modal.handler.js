function ModalHandler(_canvas) {

    var canvas = _canvas;
    var $modal = $("#modal");

    $modal.find(".cardinalityButtonHolder #openCardinalities").on("click", function() {
        var height = $(this).height();
        
        $cardinalities = $modal.find('.cardinalities');

        $cardinalities.toggle();

        $cardinalities.css("bottom", 70);

        $cardinalities.off("mouseleave");

        $cardinalities.on("mouseleave", function() {
            $(this).hide();
        });
    });

    this.openModal = function(element, mask) {
        var elementType = element.ermac.type;

        var relationships = ["many-to-many", "one-to-many", "one-to-one"];

        // detecta qual modal sera exibida
        if(relationships.indexOf(elementType) > -1) {
            elementType = "relationship";
        }

        // exibe
        var $janelaTipo = $modal.find("#" + elementType);
        $modal.find(".janelaPrincipal").removeClass("selecionado");
        $janelaTipo.addClass("selecionado");

        var filhosCanvas = canvas.children;

        carregaDados(element, mask);

        $modal.find(".cardinalityOption").off("click");
        $modal.find(".cardinalityOption").on("click", function() {
            $("#openCardinalities").text($(this).text());
            $("#openCardinalities").attr("data-cardinality", $(this).data("cardinality"));
            $cardinalities.hide();
        });

        $modal.find("#confirmarCancelar #botaoOK").off("click");
        $modal.find("#confirmarCancelar #botaoOK").on("click", function() {
            if(salvaDados(element, mask)) {

                mask.redraw();
                element.redraw();

                $modal.trigger("closeModal");
            }
        });

        $modal.trigger("openModal");
    }

    var carregaDados = function(element, mask) {
        // console.log(element);

        var $dropdownEntidadesRelacionamentos = $modal.find(".selecionado #relacionado");
        var $atributosRelacionados = $modal.find(".selecionado .relatedAttributes");
        var $atributoEsquerda = $modal.find(".selecionado .leftAttribute .entitiesSelect");
        var $atributoDireita = $modal.find(".selecionado .rightAttribute .entitiesSelect");
        var $chaveEsquerda = $modal.find(".selecionado .leftAttribute .keysSelect");
        var $chaveDireita = $modal.find(".selecionado .rightAttribute .keysSelect");

        $atributoDireita.children().remove();
        $atributoEsquerda.children().remove();


        $modal.find("#nomeElemento input").val(element.ermac.text);
        $modal.find(".selecionado #tipo").val(element.ermac.datatype);
        $modal.find(".selecionado #restricao").val(element.ermac.constraint);
        // $dropdownEntidadesRelacionamentos.val(element.ermac.relatedTo);

        $dropdownEntidadesRelacionamentos.children().remove();

        $atributoDireita.append($("<option selected/>"));
        $atributoEsquerda.append($("<option selected/>"));

        // busca por todos os relacionamento e entidades
        for (var i = 0; i < canvas.children.length; i++) {
            var canvasObj = canvas.children[i].children[0];
            if (canvasObj !== undefined && canvasObj.ermac.type !== "attribute"){

                var $option = $("<option/>");
                var $option1 = $("<option/>");
                var $option2 = $("<option/>");
                
                if (canvasObj.ermac.text === element.ermac.relatedTo) {
                    $option.attr("selected", "true");
                }

                var relationships = ["relationship", "many-to-many", "one-to-many", "one-to-one"];
                if(relationships.indexOf(element.ermac.type) > -1 && element.ermac.attributes.length !== 0) {
                    if(element.ermac.attributes[0] !== undefined && canvasObj.ermac.text === element.ermac.attributes[0].ermac.text) {
                        $option2.attr("selected", "true");
                    }

                    if(element.ermac.attributes[1] !== undefined && canvasObj.ermac.text === element.ermac.attributes[1].ermac.text) {
                        $option1.attr("selected", "true");
                    }
                }

                $option.attr("value", canvasObj.ermac.text);
                $option.text(canvasObj.ermac.text);
                $option.data("object", canvasObj);

                $option1.attr("value", canvasObj.ermac.text);
                $option1.text(canvasObj.ermac.text);
                $option1.data("object", canvasObj);

                $option2.attr("value", canvasObj.ermac.text);
                $option2.text(canvasObj.ermac.text);
                $option2.data("object", canvasObj);


                $dropdownEntidadesRelacionamentos.append($option);

                if(canvasObj.ermac !== element.ermac && canvasObj.ermac.type === "entity") {
                    $atributoDireita.prepend($option1);
                    $atributoEsquerda.prepend($option2);
                }
            }
        };

        $atributoDireita.off("change");
        $atributoEsquerda.off("change");

        $atributoDireita.on("change", function() {
            var $selecionadoDireita = $(this).find(":selected").data("object");

            $chaveDireita.children().remove();

            for(var i = 0; i < $selecionadoDireita.ermac.attributes.length; i++) {
                var $novoAtributo = $("<option/>");
                $novoAtributo.attr("value", $selecionadoDireita.ermac.attributes[i].ermac.text);
                $novoAtributo.text($selecionadoDireita.ermac.attributes[i].ermac.text);
                $chaveDireita.append($novoAtributo);
            }

        });

        $atributoEsquerda.on("change", function() {
            var $selecionadoEsquerda = $(this).find(":selected").data("object");

            $chaveEsquerda.children().remove();

            for(var i = 0; i < $selecionadoEsquerda.ermac.attributes.length; i++) {
                var $novoAtributo = $("<option/>");
                $novoAtributo.text($selecionadoEsquerda.ermac.attributes[i].ermac.text);
                $chaveEsquerda.append($novoAtributo);
            }

        });


        $modal.find(".selecionado #openCardinalities").attr("data-cardinality", element.ermac.type);
        switch(element.ermac.type) {
            case "relationship":
            case "one-to-one":
            $modal.find(".selecionado #openCardinalities").text("1 x 1");
            break;
            case "one-to-many":
            $modal.find(".selecionado #openCardinalities").text("1 x N");
            break;
            case "many-to-many":
            $modal.find(".selecionado #openCardinalities").text("N x N");
        }

        //Remove todos os atributos que já estão na modal
        $atributosRelacionados.children().remove();

        //Carrega os atributos relacionados a entidade/relacionamento
        if(element.ermac.type !== "attribute") {

            var deletaAtributo = function(atributo) {

                atributo.relatedTo = "none";
                atributo.relatedToObject = null;

                for(var j = 0; j < atributo.line.length; j++) {
                    atributo.line[j].remove();
                }

                atributo.line = [];

                for(var j = 0; ; j++) {
                    if(element.ermac.attributes[j] !== undefined && 
                        element.ermac.attributes[j].ermac.text === atributo.text) {
                            element.ermac.attributes[j] = undefined;
                        // element.ermac.attributes.splice(j, 1);
                        // console.log(element.ermac.attributes);
                        break;
                    }
                }

                mask.redrawLines();

            };

            for(var i = 0; i < element.ermac.attributes.length; i++) {
                if(element.ermac.attributes[i] !== undefined && element.ermac.attributes[i].ermac.type === "attribute") {

                    var $atributo = element.ermac.attributes[i];

                    var $linhaAtributo = $("<div/>");
                    $linhaAtributo.addClass("line");

                    var $nomeAtributo = $("<div/>");
                    $nomeAtributo.addClass("attributeName");
                    $nomeAtributo.append($atributo.ermac.text);

                    var $opcoesAtributo = $("<div/>");
                    $opcoesAtributo.addClass("attributeOptions");
                    $opcoesAtributo.append($atributo.ermac.datatype);

                    var $botaoDeletar = $("<span/>");
                    $botaoDeletar.text("X");
                    $botaoDeletar.addClass("botaoDeletar");
                    $botaoDeletar.off("click");

                    $botaoDeletar.on("click", function() {
                        deletaAtributo($atributo.ermac);
                        $linhaAtributo.remove();
                    });

                    $opcoesAtributo.append($botaoDeletar);

                    $linhaAtributo.append($nomeAtributo);
                    $linhaAtributo.prepend($opcoesAtributo);

                    $atributosRelacionados.append($linhaAtributo);
                }
            }

        }

    }

    var salvaDados = function(element, mask) {
        var nomeElemento = $modal.find('#nomeElemento input').val();

        //Checa se o nome é vazio ou contém apenas espaços
        if(nomeElemento.replace(/^\s+|\s+$/g,"") === "") {
            alert("Nome inválido.")
            return false;
        }


        //Checa se está colocando nome repetido
        for (var i = 0; i < canvas.children.length; i++) {
            var canvasObj = canvas.children[i].children[0];
            if(canvasObj !== undefined && canvasObj.ermac !== element.ermac && canvasObj.ermac.text === nomeElemento) {
                alert("Nome indisponível");
                return false;
            }
        }


        mask.children[1].text = nomeElemento;
        element.ermac.text = nomeElemento;

        element.ermac.datatype = $modal.find(".selecionado #tipo").val();
        element.ermac.constraint = $modal.find(".selecionado #restricao").val();
        element.ermac.relatedTo = $modal.find(".selecionado #relacionado").val();


        var relatedToObjectMask;

        // busca por todos os relacionamento e entidades
        for (var i = 0; i < canvas.children.length; i++) {
            var canvasObj = canvas.children[i].children[0];

            if (canvasObj !== undefined && canvasObj.ermac !== undefined && canvasObj.ermac.text === element.ermac.relatedTo) { 
                element.ermac.relatedToObject = canvas.children[i];
                break;
            }
        };

        if(element.ermac.relatedToObject !== null) {

            //Apaga as linhas que saem do elemento
            for(var i = 0; i < element.ermac.line.length; i++) {
                element.ermac.line[i].remove();
            };

            //Cria a nova linha
            var newLine = canvas.display.line({
                start: {
                    x : mask.x,
                    y : mask.y
                },
                end: {
                    x : element.ermac.relatedToObject.x,
                    y : element.ermac.relatedToObject.y
                },
                stroke: "2px black"
            });

            //Coloca a nova linha dentro do elemento ermac
            element.ermac.line.push(newLine);

            //Desenha a nvoa linha
            newLine.add();

            //Traz os elementos interligados
            element.ermac.relatedToObject.zIndex = "front";
            mask.zIndex = "front";

        }

        //Adiciona o atributo ao elemento selecionado
        for (var i = 0; i < canvas.children.length; i++) {

            var canvasObj = canvas.children[i].children[0];

            if (canvasObj !== undefined && canvasObj.ermac.type !== "attribute"){
                if (canvasObj.ermac.text === element.ermac.relatedTo) {

                    // verifica se o elemento que sera inserido ja esta no vetor de atributos
                    var existe = function exist (attributes) {
                        for (var i = 0; i < attributes.length; i++) {
                            if (attributes[i].ermac.text === element.ermac.text) {
                                return true;
                            }
                        };
                        return false;
                    };

                    if (!existe(canvasObj.ermac.attributes)) {
                        if(canvasObj.ermac.type === "entity")
                            canvasObj.ermac.attributes.push(element);
                        else {
                            for(var j = 2; ; j++) {
                                if(canvasObj.ermac.attributes[j] === undefined) {
                                    canvasObj.ermac.attributes[j] = element;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        };


        var relationships = ["relationship", "many-to-many", "one-to-many", "one-to-one"];
        if(relationships.indexOf(element.ermac.type) > -1) {
            // try {
                element.ermac.type =  $(".selecionado #openCardinalities").attr("data-cardinality");

                if(element.ermac.attributes[0] !== undefined && element.ermac.attributes[1] !== undefined) {
                    for(var i = 0; i < element.ermac.attributes[0].ermac.line.length; i++) {
                        element.ermac.attributes[0].ermac.line[i].remove();
                    }
                    element.ermac.attributes[0].ermac.line = [];

                    for(var i = 0; i < element.ermac.attributes[1].ermac.line.length; i++) {
                        element.ermac.attributes[1].ermac.line[i].remove();
                    }
                    element.ermac.attributes[1].ermac.line = [];
                }

                var $atributoEsquerda = $modal.find(".selecionado .leftAttribute .entitiesSelect");
                var $atributoDireita = $modal.find(".selecionado .rightAttribute .entitiesSelect");
                var $selecionadoDireita = $atributoDireita.find(":selected").data("object");
                var $selecionadoEsquerda = $atributoEsquerda.find(":selected").data("object");

                $selecionadoDireita.ermac.relatedTo = element.ermac.text;
                $selecionadoDireita.ermac.relatedToObject = mask;
                $selecionadoEsquerda.ermac.relatedTo = element.ermac.text;
                $selecionadoEsquerda.ermac.relatedToObject = mask;


                //Cria a nova linha
                var newLine = canvas.display.line({
                    start: {
                        x : $selecionadoDireita.ermac.mask.x,
                        y : $selecionadoDireita.ermac.mask.y
                    },
                    end: {
                        x : mask.x,
                        y : mask.y
                    },
                    stroke: "2px black"
                });

                //Coloca a nova linha dentro do elemento ermac
                $selecionadoDireita.ermac.line.push(newLine);


                //Desenha a nvoa linha
                newLine.add();

                //Cria a nova linha
                var newLine2 = canvas.display.line({
                    start: {
                        x : $selecionadoEsquerda.ermac.mask.x,
                        y : $selecionadoEsquerda.ermac.mask.y
                    },
                    end: {
                        x : mask.x,
                        y : mask.y
                    },
                    stroke: "2px black"
                });

                //Coloca a nova linha dentro do elemento ermac
                $selecionadoEsquerda.ermac.line.push(newLine2);

                //Desenha a nvoa linha
                newLine2.add();

                //Traz os elementos interligados
                $selecionadoDireita.ermac.mask.zIndex = "front";
                $selecionadoEsquerda.ermac.mask.zIndex = "front";
                mask.zIndex = "front";

                element.ermac.attributes[0] = $selecionadoEsquerda;
                element.ermac.attributes[1] = $selecionadoDireita;

                console.log(element.ermac);

                return true;
            // }

            // catch (e) {
            //     alert("Escolha as duas entidades.");
            //     return false;
            // }
        }

        return true;
    };
};