/**
 * @param components vetor de objetos do canvas
 * @return String do SQL
 */
function SQLBuilder(components) {
//    console.log(components);
//    entidades = new Array();
//    relacionamentos = new Array();
    var SQL = "";
    for (var i = 0; i < components.length; i++) {
        try {
            var componente = components[i].children[0].ermac;
        } catch (e) {
            continue;
        }
        switch (componente.type) {

            case "entity":
                //Se ele não tiver relação pai, é criado, caso contrário será 
                //criado posteriormente em outro momento do código
                if (componente.relatedTo === "none" | componente.relatedTo == null) {
                    SQL += criarEntidade(componente);
//                    entidades[entidades.length] = SQL;
                }
//                entidades[entidades.length] = SQL;
                break;
            case "relashionship":
                SQL += criarRelacionamento(componente);
//                relacionamentos[relacionamentos.length] = SQL;
                break;
            case "many-to-many":
                SQL += criarRelacionamentoMuitosMuitos(componente);
//                relacionamentos[relacionamentos.length] = SQL;
                break;
            case "one-to-many":
                SQL += criarRelacionamentoUmMuitos(componente);
//                relacionamentos[relacionamentos.length] = SQL;
                break;
            case "one-to-one":
                SQL += criarRelacionamentoUmUm(componente);
//                relacionamentos[relacionamentos.length] = SQL;
                break;
        }
        SQL += "\n";
    }
//    SQL = "";
//    for (i = 0; i < entidades.length; i++) {
//        SQL += entidades[i];
//    }
//    for (i = 0; i < relacionamentos.length; i++) {
//        SQL += relacionamentos[i];
//    }
    console.log(SQL);
    return SQL;
}


/**
 * Crioa o código SQL de uma tabela.
 * 
 * @param {type} objetoErmac A entidade em questão
 * @param {type} atributosExtras atributos que serão chaves estrangeiras, em formato 'nome tipoDado [NOT NULL] [DEFAULT valor]'.
 * @param {type} chavesEstrangeiras As definições para as chaves estrangeiras, em formato 'FOREIGN KEY nome REFERENCES nomeTabela(chavePrimaria)'
 * @returns {String}
 */
function criarEntidade(objetoErmac, atributosExtras, chavesEstrangeiras) {
//    console.log(objetoErmac);
    var entidadeSQL = "\n\nCREATE TABLE `" + objetoErmac.text + "` (\n";
    var semAtributos = true;
    var semChavePrimaria = true;
    var chavesPrimarias = "\nPRIMARY KEY(";

    for (var i = 0; i < objetoErmac.attributes.length; i++) {
        atributo = objetoErmac.attributes[i].ermac;
        if (atributo.constraint === "primary-key") {
            semChavePrimaria = false;
//            encontrouChavePrimaria = true;
//            chavesPrimarias += "`" + atributo.text + "`,";
        }
        entidadeSQL += "`" + atributo.text + "` " + atributo.datatype + " NOT NULL,\n";
        semAtributos = false;
    }
    if (semAtributos || semChavePrimaria) {
        entidadeSQL += "`" + objetoErmac.text + "_id` INTEGER NOT NULL,\n";
    }
    chavesPrimarias += obterChavesPrimarias(objetoErmac);
    //Aqui é essa verificação mesmo
    if (atributosExtras != undefined && atributosExtras != null) {
        entidadeSQL += atributosExtras;
    }
//    if (encontrouChavePrimaria) {
    chavesPrimarias = removerVirgulaFinal(chavesPrimarias);
    chavesPrimarias += ")";
    entidadeSQL += chavesPrimarias;//Já está sem vírgula no fim
//    }
    if (chavesEstrangeiras != undefined) {
        entidadeSQL += "\n" + chavesEstrangeiras;
//    }
//    if (chavesEstrangeiras === undefined) {
        entidadeSQL = "\n" + removerVirgulaFinal(entidadeSQL);
    }
    entidadeSQL += "\n);\n";
    return entidadeSQL;
}

function criarRelacionamento(component) {
    return criarEntidade(component);
}

function criarRelacionamentoMuitosMuitos(component) {

    var entidadeEsquerda = component.attributes[0].ermac;
    var entidadeDireita = component.attributes[1].ermac;
    var entidadeSQL = "\n\nCREATE TABLE `" + entidadeEsquerda.text + "_" + component.text + "_" + entidadeDireita.text + "` (\n";
    var chavesPrimarias = "\nPRIMARY KEY(";

    var chavesEsquerda, chavesDireita;

    chavesEsquerda = obterChavesPrimarias(entidadeEsquerda);
    chavesPrimarias += chavesEsquerda;
    entidadeSQL += montarSQLatributos(chavesEsquerda);

    chavesDireita = obterChavesPrimarias(entidadeDireita);
    chavesPrimarias += chavesDireita;
    entidadeSQL += montarSQLatributos(chavesDireita);
    //Verifica se existem outros atributos para a tabela de junção. Observe que
    //eles também poderão compor a nova chave.
    //Por padrão, já serão todas as chaves primárias das duas entidades
    //participantes.
    for (var i = 2; i < component.attributes.length; i++) {
        atributo = component.attributes[i].ermac;
        if (atributo.constraint === "primary-key") {
            chavesPrimarias += "`" + atributo.text + "`,";
        }
        entidadeSQL += "`" + atributo.text + "` " + atributo.datatype + " NOT NULL,\n";
    }
    chavesPrimarias = removerVirgulaFinal(chavesPrimarias);
    chavesPrimarias += ")";

    entidadeSQL += chavesPrimarias + "\n";

    //Monta as chaves entrangeiras
    var aux;
    var chavesEstrangeiras = "";

    var SQL_entidadeEsquerda;
    aux = montarSQLchavesEstrangeiras(chavesEsquerda, entidadeEsquerda);
    SQL_entidadeEsquerda = criarEntidade(entidadeEsquerda, null, null);
    chavesEstrangeiras += aux;
    var SQL_entidadeDireita;
    aux = montarSQLchavesEstrangeiras(chavesDireita, entidadeDireita);
    SQL_entidadeDireita = criarEntidade(entidadeDireita, null, null);
    chavesEstrangeiras += aux;

    entidadeSQL += chavesEstrangeiras + "\n";
    entidadeSQL += ");\n";


    return SQL_entidadeEsquerda + "\n" + SQL_entidadeDireita + "\n" + entidadeSQL;

}
function criarRelacionamentoUmMuitos(component) {

    var entidadeSQL = "";
    var entidadeUm = component.attributes[0].ermac;
    var entidadeMuitos = component.attributes[1].ermac;

    entidadeSQL += criarEntidade(entidadeUm);

    var chavePrimaria = obterChavesPrimarias(entidadeUm);
    var chaveEstrangeira = montarSQLchavesEstrangeiras(chavePrimaria, entidadeUm);
    var novasOpcoes = montarSQLatributos(chavePrimaria);

    entidadeSQL += criarEntidade(entidadeMuitos, novasOpcoes, chaveEstrangeira);

    return entidadeSQL;
}
function criarRelacionamentoUmUm(component) {
    var entidadeSQL = "";
    var entidadeUm = component.attributes[0].ermac;
    var entidadeOutroUm = component.attributes[1].ermac;

    var chavePrimaria1 = obterChavesPrimarias(entidadeUm);
    var chavePrimariaO1 = obterChavesPrimarias(entidadeOutroUm);

    var chaveEstrangeira;
    var novasOpcoes;
    var valorSorteado = Math.random();
    if (valorSorteado > 0.5) {
        entidadeSQL += criarEntidade(entidadeOutroUm);
        chaveEstrangeira = montarSQLchavesEstrangeiras(chavePrimariaO1, entidadeOutroUm);
        novasOpcoes = montarSQLatributos(chavePrimaria1);

        entidadeSQL += criarEntidade(entidadeUm, novasOpcoes, chaveEstrangeira);
    } else {
        entidadeSQL += criarEntidade(entidadeUm);
        chaveEstrangeira = montarSQLchavesEstrangeiras(chavePrimaria1, entidadeUm);
        novasOpcoes = montarSQLatributos(chavePrimaria1);

        entidadeSQL += criarEntidade(entidadeOutroUm, novasOpcoes, chaveEstrangeira);
    }
    return entidadeSQL;
}

////////////////////////////////////////////////////////////////////////////////
//  Funções auxiliares utilizadas ao longo dos métodos desse arquivo    ////////
////////////////////////////////////////////////////////////////////////////////
/**
 * Retorna uma string no formato:
 * 
 * chave1,chave2,...,chaven,
 * 
 * Detalhe para a vírgula no final.
 * 
 * @param {Ermac} objetoErmac
 * @returns {String} chaves formatadas
 */
function obterChavesPrimarias(objetoErmac) {
    chaves = "";
    for (i = 0; i < objetoErmac.attributes.length; i++) {
        if (objetoErmac.attributes[i].ermac.constraint === "primary-key") {
            chaves += "`" + objetoErmac.attributes[i].ermac.text + "`,";
        }
    }
    if (chaves === "") {
        chaves = objetoErmac.text + "_id";
    }
    return chaves;
}
/**
 * Como o nome sugere, remove a última ocorrência de uma vírgula no texto, caso
 * exista. Se não, a string original é retornada.
 * 
 * @param {type} string Palavra para remover a última vírgula
 */
function removerVirgulaFinal(string) {
    var posicao = string.lastIndexOf(",");
    if (posicao > 0) {
        return string.substr(0, posicao);
    } else {
        return string;
    }
}

/**
 * Monta uma linha configurada em SQL de atributos
 * @param {type} string Uma entrada no formato chave1,chave2,...,chaven,
 * @returns {undefined} O SQL necessário, não fechado, para adicionar ao meio de um código já existente.
 */
function montarSQLatributos(string) {
    string = removerVirgulaFinal(string);
    var chaves = new Array();
    chaves = string.split(",");
    var SQL = "";
    for (i = 0; i < chaves.length; i++) {
        SQL += "`" + chaves[i] + "` VARCHAR NOT NULL,\n";
    }
    return SQL;
}
/**
 * 
 * @param {type} chaves As chaves primárias da entidade alvo.
 * @param {type} entidadeAlvo Entidade alvo
 * @returns {String} Comandos SQL 
 */
function montarSQLchavesEstrangeiras(chaves, entidadeAlvo) {
    chaves = removerVirgulaFinal(chaves);
    var chavesSeparadas = new Array();
    chavesSeparadas = chaves.split(",");
    var SQL = "";
    for (i = 0; i < chavesSeparadas.length; i++) {
        SQL += "FOREIGN KEY(`" + chavesSeparadas[i] + "`) REFERENCES `" + entidadeAlvo.text + "`(`" + chavesSeparadas[i] + "`),\n";
    }
    return SQL;
}