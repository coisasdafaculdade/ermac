/**
 * Fabrica de componentes no canvas
 * @param oCanvas instancia do canvas
 */
 function ComponentFactory(oCanvas) {
    // mantem uma copia local do canvas instanciado
    this.canvas = oCanvas;
    var _ = this;
    var modal = new ModalHandler(oCanvas);

    // Objeto de parametros da função dragAndDrop
    var dragOption = {
        changeZindex: true,
        start : function(){
            selectorEnabled  = true;
            selectionEnabled = true;
        },
        move : function() {
            if(selectionEnabled) {
                moveGrid(this);
            }
            
        },
        end : function(){
            selectorEnabled  = false;
            selectionEnabled = false;
        }
    };

    /**
     * Metodo padrão para mover objetos em grid.
     * @param object Objeto alvo que será movido
     */
     var moveGrid = function(object) {
        // limite maximo de deslocamento (20px)
        // limiar de deslocamento (sempre limite / 2)

        // console.log(object);

        var limit = 20;
        var threshold = limit / 2;

        var mod = {
            x : (object.x % limit),
            y : (object.y % limit)
        };
        
        object.x += (mod.x > threshold) ? limit - (mod.x % limit) : threshold - (mod.x % limit);
        object.y += (mod.y > threshold) ? limit - (mod.y % limit) : threshold - (mod.y % limit);

        if(object.children[0].ermac.relatedToObject !== null)
            object.redrawLines();

        for(var i = 0; i < object.children[0].ermac.attributes.length; i++) {
            if(object.children[0].ermac.attributes[i] !== undefined) {
                object.children[0].ermac.attributes[i].ermac.mask.redrawLines();
            }
        }
    };

    var openModal = function(element, mask) {
        modal.openModal(element, mask);
    }

    /**
     * Cria uma mascara para o componente desejado.
     * O tipo do atributo sera usado na criação do objeto Ermac
     * @return Componente mascara
     */
     var createMask = function(coord) {
        var X = canvas.width  / 2;
        var Y = canvas.height / 2;

        if (coord) {
            X = parseInt(coord.x);
            Y = parseInt(coord.y);
        }

        var mask = canvas.display.rectangle({
            name : "",
            x: X,
            y: Y,
            height: 40,
            width : 100,
            fill  : "rgba(255,255,255, 0)",
            origin: { x: "center", y: "center" },
            redrawLines : function() {
                var that = this;

                if(that.children[0].ermac.line.length !== 0) {

                    for(var i = 0; i < that.children[0].ermac.line.length; i++) {
                        that.children[0].ermac.line[i].remove();
                    };

                    that.children[0].ermac.line = [];

                    //Cria a nova linha
                    var newLine = canvas.display.line({
                        start: {
                            x : that.x,
                            y : that.y
                        },
                        end: {
                            x : that.children[0].ermac.relatedToObject.x,
                            y : that.children[0].ermac.relatedToObject.y
                        },
                        stroke: "2px black"
                    });

                    //Coloca a nova linha dentro do elemento ermac
                    that.children[0].ermac.line.push(newLine);

                    //Desenha a nvoa linha
                    newLine.add();

                    //Traz os elementos interligados
                    that.children[0].ermac.relatedToObject.zIndex = "front";
                    that.zIndex = "front";
                }
            }
        });

        return mask;
    };

    /**
     * Cria o texto padrão de cada componente.
     * @param defaultText Texto padrão para exibir
     * @return Componente de texto
     */
     var createText = function(defaultText) {
        return canvas.display.text({
            origin: { x: "center", y: "center" },
            font: "bold 15px Arial",
            text: defaultText,
            fill: "white"
        }); 
    };

    this.createEntity = function() {
        var objectType = "entity";
        var mask = createMask();
        var name = "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.rectangle({
            height: 40,
            width: 100,
            fill: "#5da423",
            stroke: "inside 2px #219421",
            origin: { x: "center", y: "center" },
            ermac: new Ermac(name, objectType, mask)
        });

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        mask.addChild(text);

        text.bind("dblclick", function() {

            openModal(object, mask);
        });

        object.bind("dblclick", function() {
            openModal(object, mask);
        });

        return mask;
    };

    this.createWeakEntity = function() {
        return null;
    };

    this.createAttribute = function() {
        var objectType = "attribute";
        var mask = createMask();
        var name = "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.ellipse({
            radius_x: 50,
            radius_y: 20,
            fill: "#3498db",
            stroke: "inside 1px #217dbb",
            origin: { x: "center", y: "center" },
            ermac : new Ermac(name, objectType, mask)
        });

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        mask.addChild(text);

        text.bind("dblclick", function() {
            openModal(object, mask);
        });

        object.bind("dblclick", function() {
            openModal(object, mask);
        });
        
        return mask;
    };

    this.createMultivaluedAttribute = function() {
        return null;
    };

    this.createDerivedAttribute = function() {
        return null;
    };

    this.createRelationship = function() {
        var objectType = "relationship";
        var mask = createMask();
        var name = "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.polygon({
            origin: { x: "center", y: "center" },
            sides: 4,
            rotation: 90,
            side: 28.28,
            scalingY: 2.501,
            fill: "#e74c3c",
            stroke: "inside 1px #d62c1a",
            ermac : new Ermac(name, objectType, mask)
        });

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        // gambiarra dos deuses!
        // o bendito texto fica todo bugado
        // quando adicionado como filho de
        // 'object' por conta do scalingY :'(
            mask.addChild(text);

            text.bind("dblclick", function() {
                openModal(object, mask);
            });

            object.bind("dblclick", function() {
             openModal(object, mask);
         });

        return mask;
    };

    this.createWeakRelationship = function() {
        return null;
    };






    // CLASSES USADAS PARA ADICIONAR AO
    // CANVAS OBJETOS JSON RECUPERADOS DO BANCO

    this.recreateAttribute = function(newObject) {
        var coord = {
            x : newObject.x,
            y : newObject.y
        };
        
        var objectType = newObject.attribute || "attribute";
        var mask = createMask(coord);
        var name = newObject.text || "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.ellipse({
            radius_x: 50,
            radius_y: 20,
            fill: "#3498db",
            stroke: "inside 1px #217dbb",
            origin: { x: "center", y: "center" },
            ermac : new Ermac(name, objectType, mask)
        });

        if (newObject) {
            object.ermac.datatype = newObject.datatype;
            object.ermac.constraint = newObject.constraint;
        }

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        mask.addChild(text);

        text.bind("dblclick", function() {
            openModal(object, mask);
        });

        object.bind("dblclick", function() {
            openModal(object, mask);
        });

        return mask;
    }

    this.recreateEntity = function(newObject) {
        var coord = {
            x : newObject.x,
            y : newObject.y
        };

        var objectType = newObject.type || "entity";
        var mask = createMask(coord);
        var name = newObject.text || "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.rectangle({
            height: 40,
            width: 100,
            fill: "#5da423",
            stroke: "inside 2px #219421",
            origin: { x: "center", y: "center" },
            ermac: new Ermac(name, objectType, mask)
        });

        if (newObject) {
            object.ermac.datatype = newObject.datatype;
            object.ermac.constraint = newObject.constraint;
        }

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        mask.addChild(text);

        text.bind("dblclick", function() {
            openModal(object, mask);
        });

        object.bind("dblclick", function() {
            openModal(object, mask);
        });

        return mask;
    };

    this.recreateRelationship = function(newObject) {
        var coord = {
            x : newObject.x,
            y : newObject.y
        };

        var objectType = newObject.type || "relationship";
        var mask = createMask(coord);
        var name = newObject.text || "unamed" + mask.id;
        var text = createText(name);

        var object = canvas.display.polygon({
            origin: { x: "center", y: "center" },
            sides: 4,
            rotation: 90,
            side: 28.28,
            scalingY: 2.501,
            fill: "#e74c3c",
            stroke: "inside 1px #d62c1a",
            ermac : new Ermac(name, objectType, mask)
        });

        if (newObject) {
            object.ermac.datatype = newObject.datatype;
            object.ermac.constraint = newObject.constraint;
        }

        mask.addChild(object);

        mask.dragAndDrop(dragOption);

        // gambiarra dos deuses!
        // o bendito texto fica todo bugado
        // quando adicionado como filho de
        // 'object' por conta do scalingY :'(
        mask.addChild(text);

        text.bind("dblclick", function() {
            openModal(object, mask);
        });

        object.bind("dblclick", function() {
            openModal(object, mask);
        });

        return mask;
    };


};