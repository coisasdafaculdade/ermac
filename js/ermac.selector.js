// espaco entre a selecao e o objeto
var SELECTION_GAP = 0;
// tamanho do quadrado da alça de seleção
var HANDLE_SIZE = 7;
// sombreado (outer glow) da alça de seleção
var HANDLE_SHADOW = "0 0 1px 1px #CCC";
// cor interna da alça de seleção
var HANDLE_FILL = "#EEE";

/**
 * Cria retangulo de seleção
 * @param selectedObjects vetor de objetos selecionados
 * @return Retangulo de seleção
 */
function createSelection(selectedObjects) {

    if(!selectedObjects || selectedObjects.length === 0) {
        return null;
    }

    // estrutura para armazenar os pontos
    // mais extremos da seleção
    var area = {
        min : {
            x : Number.MAX_VALUE,
            y : Number.MAX_VALUE,
            width  : 0,
            height : 0,
        },
        max : {
            x : Number.MIN_VALUE,
            y : Number.MIN_VALUE,
            width  : 0,
            height : 0
        }
    };

    // Busca entre todos os objetos selecionados,
    // as coordenadas mais extremas.
    for(var i = 0; i < selectedObjects.length; i++) {

        // traz o objeto para frente do canvas
        selectedObjects[i].zIndex = "front";

        var object = selectedObjects[i];
    
        if(object.x < area.min.x) {
            area.min.x = object.x;
            area.min.width = object.width;
        }
        if(object.x > area.max.x) {
            area.max.x = object.x;
            area.max.width = object.width;
        }

        if(object.y < area.min.y) {
            area.min.y = object.y;
            area.min.height = object.height;
        }
        if(object.y > area.max.y) {
            area.max.y = object.y;
            area.max.height = object.height;
        }
    }

    // ajusta a real dimensão do retangulo
    var dimension = {
        x : area.min.x - ((area.min.width  / 2) + SELECTION_GAP),
        y : area.min.y - ((area.min.height / 2) + SELECTION_GAP),
        width : (area.max.x + area.max.width)  - area.min.x + SELECTION_GAP,
        height: (area.max.y + area.max.height) - area.min.y + SELECTION_GAP,
    };

    var selection = canvas.display.rectangle({
        x : dimension.x,
        y : dimension.y,
        width : dimension.width,
        height: dimension.height,
        origin: { x: 0, y: 0 },
        fill   : "rgba(255, 255, 255, 0)",
        stroke : "1px #18a",
        old    : { x : dimension.x, y : dimension.y },
        selected : selectedObjects,
        zIndex : "front"
    })
    .bind("mouseenter", function() {
        canvas.mouse.cursor("move");
    })
    .bind("mouseleave", function () {
        canvas.mouse.cursor("default")
    });

/** Alças de seleção estão desativadas até segunda ordem.
    var nwResize = canvas.display.rectangle({
        x: -HANDLE_SIZE,
        y: -HANDLE_SIZE,
        width : HANDLE_SIZE,
        height: HANDLE_SIZE,
        fill   : HANDLE_FILL,
        shadow : HANDLE_SHADOW
    }).bind("mouseenter", function(){
        canvas.mouse.cursor("nw-resize")
    });

    var neResize = canvas.display.rectangle({
        x: selection.width,
        y: -HANDLE_SIZE,
        width : HANDLE_SIZE,
        height: HANDLE_SIZE,
        fill   : HANDLE_FILL,
        shadow : HANDLE_SHADOW
    }).bind("mouseenter", function(){
        canvas.mouse.cursor("ne-resize")
    });

    var swResize = canvas.display.rectangle({
        x:-HANDLE_SIZE,
        y: selection.height,
        width : HANDLE_SIZE,
        height: HANDLE_SIZE,
        fill   : HANDLE_FILL,
        shadow : HANDLE_SHADOW
    }).bind("mouseenter", function(){
        canvas.mouse.cursor("sw-resize")
    });

    var seResize = canvas.display.rectangle({
        x: selection.width,
        y: selection.height,
        width : HANDLE_SIZE,
        height: HANDLE_SIZE,
        fill   : HANDLE_FILL,
        shadow : HANDLE_SHADOW
    }).bind("mouseenter", function(){
        canvas.mouse.cursor("se-resize")
    });
*/

    selection
    .dragAndDrop({
        changeZindex : true,
        start : function(){
            selectorEnabled = true;
            selectionEnabled = true;
        },
        move : function() {

            // limite maximo de deslocamento (20px)
            // limiar de deslocamento (sempre limite / 2)
            var limit = 20;
            var threshold = limit / 2;

            // modulo do deslocamento
            var mod = {
                x : (this.x % limit),
                y : (this.y % limit)
            };

            this.x += (mod.x > threshold) ? limit - (mod.x % limit) : threshold - (mod.x % limit);
            this.y += (mod.y > threshold) ? limit - (mod.y % limit) : threshold - (mod.y % limit);

            // deslocamento dos atributos relacionados a seleção
            var gap = {
                x : this.x - this.old.x,
                y : this.y - this.old.y
            }

            for(i = 0; i < this.selected.length; i++) {
                var object = this.selected[i];
                object.move(gap.x, gap.y);
            }

            this.old.x = this.x;
            this.old.y = this.y;
        },
        end : function(){
            selectorEnabled  = false;
            selectionEnabled = false;
        }
    });

    return selection;
}

/**
 * Detecta quais objetos estao dentro da area de seleção
 * @param selectorArea Retangulo de seleção do canvas
 * @param allCanvasObjects Vetor de todos os objetos existentes no canvas
 * @return Todos os objetos que estao dentro da area de seleção,
 * um vetor vazio é retornado se não houver nenhum objeto dentro da area
 */
function getSelectedObjects(selectorArea, allCanvasObjects) {
    var selectedObjects = [];
    
    if(!selectorArea || !allCanvasObjects || allCanvasObjects.length === 1) {
        return selectedObjects;
    }
    
    var rectA = {
        x1 : selectorArea.x,
        y1 : selectorArea.y,
        x2 : selectorArea.x + selectorArea.width,
        y2 : selectorArea.y + selectorArea.height
    };

    /**
     * Percorremos o vetor ate a posicao length - 1 pois o
     * ultimo elemento adicionando ao canvas neste momento,
     * sempre sera o retangulo de seleção (selectorArea).
     */
    for(var i = 0; i < allCanvasObjects.length - 1; i++) {

        var object = allCanvasObjects[i];

        var rectB = {
            x1 : object.x,
            y1 : object.y,
            x2 : object.x + object.width,
            y2 : object.y + object.height
        };

        /**
         * As coordenadas negativas da seleção 
         * sao invertidas para que seja possivel detectar
         * a intersecção dos retangulos mais facilmente.
         */
        if(rectA.x1 > rectA.x2) {
            var tmp = rectA.x1;
            rectA.x1 = rectA.x2;
            rectA.x2 = tmp;
        }

        if(rectA.y1 > rectA.y2) {
            var tmp = rectA.y1;
            rectA.y1 = rectA.y2;
            rectA.y2 = tmp;
        }

        if(rectA.x1 < rectB.x2 && rectA.x2 > rectB.x1 &&
           rectA.y1 < rectB.y2 && rectA.y2 > rectB.y1) {

            selectedObjects.push(object);
        }
    }

    return selectedObjects;
}
