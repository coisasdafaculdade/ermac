
var canvas;
var componentFactory;

var KEY_ESC = 27;
var KEY_DELETE = 46;

// variaveis de controle do seletor
var selectorArea;
var selectorEnabled = false;
var selectorDragging = false;

// variaveis de controle da seleção
var selection;
var selectionEnabled = false;
var selectionDragging = false;
var selectedObjects = [];


$(function(){
    canvas = oCanvas.create({canvas: "#canvas"});
    componentFactory = new ComponentFactory(canvas);

    $('#modal').easyModal({
        top: 50,
        overlayColor: "transparent"
    });


    canvas.bind("mousedown", function(){

        // Se o Seletor estiver disponivel, entao criamos um retangulo
        // do Seletor no canvas e atualizaremos suas dimensoes nos eventos "mousemove"
        // e apagamos o retangulo do Seletor quando o evento "mouseup" for disparado
        if(!selectorEnabled) {
            selectorDragging = true;

            selectorArea = canvas.display.rectangle({
                x: canvas.mouse.x, y: canvas.mouse.y,
                width: 0, height: 0,
                fill: "rgba(173, 207, 239, 0.5)",
                stroke: "2px rgba(49, 154, 255, .8)"
            });
            canvas.addChild(selectorArea);
        }

        // Se o usuario estiver selecionado algum objeto no canvas,
        // e clicar em cima da seleção, então ativamos o modo de
        // drag da seleção, permitindo assim mover todos objetos selecionados
        if(selection) {
            if(canvas.mouse.x > selection.x && canvas.mouse.x < selection.x + selection.width &&
               canvas.mouse.y > selection.y && canvas.mouse.y < selection.y + selection.height) {
                selectionDragging = true;
            }
        }

    });


    canvas.bind("mouseup", function(){

        // Se tivermos uma seleção e não estivermos movendo-a
        // então devemos apagar o retangulo da seleção
        if(selection && !selectionDragging) {
            selection.remove();
            selection = null;
        }

        // tenta detectar quais objetos estao na area de seleção,
        // para que sejam atribuidos no vetor de Objetos Selecionados
        selectedObjects = getSelectedObjects(selectorArea, canvas.children);
        selection = createSelection(selectedObjects) || selection;
        

        if((!selectorEnabled || selectorDragging) && selectorArea) {
            if(selection) {
                canvas.addChild(selection);
            }
            selectorArea.remove();
            selectorArea = null;
        }

        // Sempre que 'desclicar' devemos bloquear os 
        // modods de  dragging da seleção e do seletor
        selectorDragging = false;
        selectionDragging = false;
    });


    canvas.bind("mousemove", function() {
        if(!selectorEnabled && selectorDragging) {
            selectorArea.width = canvas.mouse.x - selectorArea.x;
            selectorArea.height= canvas.mouse.y - selectorArea.y; 
            selectorArea.redraw();
        }
    });


    canvas.bind("keydown", function() {
        var keys = canvas.keyboard.getKeysDown();

        if (selectedObjects.length > 0) {

            // Se a tecla pressionada for somente DELETE e houver objetos
            //selecionados entao, deletamos todos os objetos da seleção
            if (keys[0] === KEY_DELETE) {
                for(var i = 0; i < selectedObjects.length; i++) {

                    // se um elemento nao tiver filhos, significa que é um atributo
                    // entao nao precisamos remove-lo, pois seu pai o remverá
                    if (selectedObjects[i].__proto__.children.length <= 0) {
                        continue;
                    }

                    var attr = selectedObjects[i].__proto__.children[0].ermac.attributes;

                    for (var j = 0; j < attr.length; j++) {

                        console.log(attr[j]);
                        var line = attr[j].ermac.line;
                        for (var k = 0; k < line.length; k++) {
                            line[k].remove();
                        }

                        attr[j].ermac.mask.remove();
                        attr[j].remove();
                    }

                    selectedObjects[i].remove();
                }

                selection.remove();
                selection = null;
                selectedObjects = [];
            }

            // Se a tecla pressionada for somente ESC, entao apagamos a seleção
            if (keys[0] === KEY_ESC) {
                selection.remove();
                seletion = null;
                selectedObjects = [];
            }
        }
    });

});