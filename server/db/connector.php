<?php

include_once('dbconfig.php');

class DBConnector {

    private static $con = NULL;

    /**
     * Retorna a conexao usando esquema especificado nas configurações.
     * Usado para CRUD em geral.
     */
    public static function getConnection() {
        $server = SERVER;
        $username = USERNAME;
        $password = PASSWORD;
        $database = DATABASE;

        if (empty($server)   || empty($username) ||
            empty($password) || empty($database)) {
            return NULL;
        }

        if (self::$con == NULL) {
            $con = mysqli_connect($server, $username, $password, $database);
        }

        return $con;
    }

    /**
     * Retorna a conexao sem usar nenhum banco de dados.
     * Utilizado para executar scripts SQL.
     */
    public static function getSimpleConnection() {
        $server = SERVER;
        $username = USERNAME;
        $password = PASSWORD;
        $database = DATABASE;

        if (empty($server)   || empty($username) ||
            empty($password) || empty($database)) {
            return NULL;
        }

        if (self::$con == NULL) {
            $con = mysqli_connect($server, $username, $password);
        }

        return $con;
    }

}

?>