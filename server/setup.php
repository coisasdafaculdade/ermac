<?php

include_once('db/connector.php');

function import_file($filename){
	$con = DBConnector::getSimpleConnection();

	if ($con == NULL) {
		return false;
	}

    if ($file = file_get_contents($filename)){
        foreach(explode(";", $file) as $query){
            $query = trim($query);
            if (!empty($query) && $query != ";") {
                mysqli_query($con, $query);
            }
        }
    }

    mysqli_close($con);
    return true;
}

if (import_file('db/ermac.sql')) {
	echo ("Importado com sucesso.");
} else {
	echo("Falha ao criar banco de dados. Cheque seu arquivo dbconfig.php");
}

?>