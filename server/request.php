

<?php
	include_once("db/connector.php");


	if (isset($_REQUEST['CREATE'])) {

		$json = json_encode($_POST['json']);
		$json = str_replace('"', '\\"', $json);

		$con = DBConnector::getConnection();
		$query = "INSERT INTO diagrama (criacao, json) " .
				 "VALUES(CURDATE(), '". $json ."');";

        $result = mysqli_query($con, $query);
        mysqli_close($con);

        echo($result);
	}

	else if (isset($_REQUEST['SELECT'])) {
		$con = DBConnector::getConnection();
		$query = "SELECT * FROM diagrama;";
        $result = mysqli_query($con, $query);

		while($row = mysqli_fetch_array($result)) {
  			echo $row['id'] . ";" . $row['criacao'] . ";'" . $row['json'] . "'\n";
		}

        mysqli_close($con);

	}

	else if (isset($_REQUEST['DELETE'])) {
		$id = $_REQUEST['DELETE'];
		$con = DBConnector::getConnection();
		$query = "DELETE FROM diagrama WHERE id=" . $id;
        $result = mysqli_query($con, $query);
        mysqli_close($con);
     	echo($result);   
	}

?>